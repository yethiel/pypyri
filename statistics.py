import matplotlib.pyplot as plt
import ast
import datetools


""" Can be used to count occurrences of strings
"""
class FrequencyDistribution:

    def __init__(self):
        self.data = {}
        self.other = 0

    def add_occurrence(self, thing):
        if thing in self.data:
            self.data[thing] += 1
        else:
            self.data[thing] = 1

    def get_data(self):
        return self.data

    def get_total(self):
        return sum([self.data[key] for key in self.data])

    def show_histogram(self):
        pass

    def show_piechart(self):
        # Pie chart, where the slices will be ordered and plotted counter-clockwise:
        data_sorted = self.get_data_sorted()
        data_sum = sum([data_sorted[d] for d in data_sorted])
        labels = [f"{d}: {data_sorted[d]} ({(data_sorted[d]/data_sum)*100:.2f}%)" for d in data_sorted][::-1]
        total = self.get_total()
        sizes = [data_sorted[key]/total for key in data_sorted][::-1]

        fig1, ax1 = plt.subplots()
        pie = ax1.pie(sizes,shadow=False, startangle=90)
        ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
        plt.legend(pie[0],labels, bbox_to_anchor=(1,0.5), loc="center right", fontsize=10, 
           bbox_transform=plt.gcf().transFigure)

        plt.show()

    
    def show_histogram(self):
        fig = plt.figure()
        pdata = sorted(self.data)
        labels = [str("" if x%200 else datetools.int_to_date(pdata[x])) for x in range(len(self.data.values()))]
        plt.bar(range(len(pdata)), [self.data[x] for x in pdata], tick_label=labels , width=2.0) 
        # plt.bar(labels, self.data.values()) 
        # plt.bar(self.data.keys(), self.data.values(), width=2.0) 
        plt.xticks(rotation = 30) # Rotates X-Axis Ticks by 45-degrees
        plt.show()

    def get_data_sorted(self):
        if self.other:
            sorted_data = {"andere": self.other}
        else:
            sorted_data = {}
        sorted_data.update(dict(sorted(self.data.items(), key=lambda item: item[1])))
        return sorted_data

    def collapse(self, min_other: int):
        delete = []
        other_count = 0
        for key in self.data:
            if self.data[key] < min_other:
                other_count += self.data[key]
                delete.append(key)

        for key in delete:
            del self.data[key]
        self.other = other_count


    def __repr__(self): 
        # sorted_data = self.get_data_sorted()
        sorted_data = self.data
        
        out = ""
        total = 0
        for key in sorted_data:
            out += f"{key}:\t{sorted_data[key]}\n"
            total += sorted_data[key] 

        out += f"\nTotal: {total}"

        return out