from bs4 import BeautifulSoup

class Metadata:


    def __init__(self):
        self.fields = {}

 """ Finds all te* tags. 
        There can be multiple ones since every language has its own entry
        filter_lang: A list of language codes can be provided to only return tags 
        that have the language property and a value from the list
    """
    def from_soup(self, soup, filter_lang=[]):
        for field in self.fields:
            tags = soup.findAll(self.fields[field])
            if tags:
                [setattr(self, field, t.get_text()) for t in tags]
                
