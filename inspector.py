import os
import json
from structure import *
from statistics import *
import numpy as np
import datetools

""" This file contains some test and research functions used in the project 
    and for testing the modules
"""


def get_objects_with_fragments(min_num_fragments: int=1):

    for fname in os.listdir("corpus"):
        fpath = os.path.join("corpus", fname)

        # This is either a complete structure (written object including text data and fragment data)
        with open(fpath, "r") as f:
            struc = Structure(f)

            if (len(struc.get_fragments()) >= min_num_fragments):
                print(fname)


def get_objects_with_text_data(min_num_text_data: int=1):
    for fname in os.listdir("corpus"):
        fpath = os.path.join("corpus", fname)

        # This is either a complete structure (written object including text data and fragment data)
        with open(fpath, "r") as f:
            struc = Structure(f)

            if (len(struc.get_text_data()) >= min_num_text_data):
                print(fname)




""" Iterates over all objects and counts the occurrence of metadata values

    dataset: "text" or "object"
    tag: readable tag ("title" as opposed to st01, etc.) to count
    filter: dictionary with metadata values to filter for
    aggregated: True when combinations of metadata value occurrences should be counter, False for single occurrences
"""
def get_frequency_metadata(dataset: str, tag: str, filter: dict={}, aggregated=False, min_other=0):
    
    freq = FrequencyDistribution()

    not_defined_count = 0
    not_defined_ids = []

    # Opens the corpus file to read JSON data from
    with open("corpus.json") as fj:
        data = json.load(fj)

        # iterates over all IDs
        for internal_id in data:

            # dataset is either "text" or "object"
            metadata = data[internal_id][dataset]
            if tag in metadata:

                if evaluate_filter(data[internal_id], filter):
                    if aggregated:
                        if type(metadata[tag]) is list:
                            for occ in metadata[tag]:
                                freq.add_occurrence(occ.replace("?", "").strip())
                        else:
                            freq.add_occurrence(metadata[tag])
                    else:
                        if type(metadata[tag]) is list:
                            freq.add_occurrence(", ".join(sorted(metadata[tag])))
                        else:
                            freq.add_occurrence(metadata[tag])

            else:
                not_defined_count += 1
                not_defined_ids.append(internal_id)
    print(freq)
    print(f"Tag not found in {not_defined_count} objects")
    # print(not_defined_ids)
    if min_other > 0:
        freq.collapse(min_other)

    return freq
    
""" Iterates over all objects and plots their time interval.
"""
def get_time_distribution():

    freq = FrequencyDistribution()

    not_defined_count = 0
    not_defined_ids = []



    # Opens the corpus file to read JSON data from
    with open("corpus.json") as fj:
        data = json.load(fj)

        # iterates over all IDs
        for internal_id in data:
            metadata = data[internal_id]
            if "text" in metadata:
                if "date_int_from" in metadata["text"] and "date_int_to" in metadata["text"]:
                    date_from = metadata["text"]["date_int_from"]
                    date_to = metadata["text"]["date_int_to"]
                    for x in range(date_from, date_to):
                        if not (x % 1000):
                            # years.append(years)
                            freq.add_occurrence(x)

    min_date = min(freq.data.keys())
    max_date = max(freq.data.keys())

    for x in range(min_date, max_date):
        if not x in freq.data and not (x%500):
            freq.data[x] = 0

    # pdata = sorted(freq.data)

    # years = pdata
    # frequency = [freq.data[x] for x in pdata]
    # plt.plot(frequency, years)
    # plt.tight_layout()
    # # plt.fill_between(years, frequency)
    # plt.show()

    return freq
            
def evaluate_filter(object_data, filters):
    for dataset_key in filters:
        for tag_key in filters[dataset_key]:
            if not object_data[dataset_key][tag_key] or filters[dataset_key][tag_key] not in object_data[dataset_key][tag_key]:
                return False
    
    return True


def get_frequency_text_metadata(tag, filters: dict={}, aggregated=False, min_other=0):
    dataset = "text"
    return get_frequency_metadata(dataset, tag, filters, aggregated, min_other)


def get_frequency_object_metadata(tag, filters: dict={}, aggregated=False, min_other=0):
    dataset = "object"
    return get_frequency_metadata(dataset, tag, filters, aggregated, min_other)


def ew_add_all(nparray):
    if not nparray:
        return
    arr = nparray[0]

    for a in nparray[1:]:
        arr+=a
    return arr

def stacked_chart():
    materials = np.array([m for m in get_frequency_object_metadata("format").get_data().keys()])
    languages = np.array([m for m in get_frequency_text_metadata("language").get_data().keys()])
    print(materials)

    langdict = {}

    for lang in languages:
        languisch = np.array([sum(get_frequency_text_metadata("language", {"object": {"format": material}, "text":{"language": lang}}).get_data().values()) for material in materials])
        langdict[lang] = languisch

    other = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
    
    for lang in languages:
        if sum(langdict[lang]) < 10:
            other += langdict[lang]
            del langdict[lang]
    langdict["andere"] = other
    ind = [x for x, _ in enumerate(materials)]

    lkeys = list(langdict.keys())

    for langkey in lkeys:
        langkey_index = lkeys.index(langkey)
        bottom = ew_add_all([langdict[l] for l in lkeys[langkey_index:] if l != langkey])
        plt.bar(ind, langdict[langkey], width=0.8, label=langkey, bottom=bottom)


    plt.xticks(ind, materials)
    plt.ylabel("Objekte")
    plt.xlabel("Ausführung")
    plt.legend(loc="upper right")
    plt.title("")

    plt.show()



                

# with open("corpus/UBLPapyri_schrift_00299110.xml", "r") as f:
#     struc = Structure(f)

# stacked_chart()

# Places
# freq = get_frequency_text_metadata("place", min_other=5, aggregated=True)
# freq.data["keine Angabe bekannnt"] = 0
# freq.show_piechart()




get_frequency_text_metadata("place").show_piechart()

# get_frequency_text_metadata("type", min_other=50, aggregated=True).show_piechart()
# get_frequency_object_metadata("state_preservation", filters={"text": {"language": "Griechisch"}}).show_piechart()
# get_frequency_object_metadata("material", aggregated=False).show_piechart()

# get_time_distribution().show_histogram()