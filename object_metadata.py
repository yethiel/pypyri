from metadata import Metadata


""" Class for general written object metadata
    Accessible as st* tags (Schriftträger)
"""
class ObjectMetadata(Metadata):

    def __init__(self):


        self.fields = {
            "st01": "title",
            "st02": "inventory_number",
            "st03": "other_ordering_systems",
            "st04": "collection",
            "st05": "placement",
            "st06": "acquisition",
            "st07": "publication",
            "st08": "publication_number",
            "st09": "material",
            "st10": "color",
            "st11": "color_description",
            "st12": "dimensions",
            "st13": "state_preservation",
            "st14": "complete",
            "st15": "custodial_information",
            "st16": "convservation",
            "st17": "format",
            "st18": "reconstructed_size",
            "st19": "colleseis",
            "st20": "physical_joins",
            "st21": "external_link",
            "st22": "physical_joins_remarks_interal",
            "st23": "physical_joins_remarks_internal_links",
            "st24": "publication_type",
            "st25": "?",
            "st26": "?",
            "st27": "?",
            "st28": "?",
            "st29": "?",
            "st30": "images_published",
            "st31": "images_collections",
            "st32": "remarks",
            "st33": "remarks_internal",
            "st34": "additional_links",
            "st35": "author_record",
            "st36": "licenses",
            "st37": "alternate_id",
            "st38": "internal_id",

        }