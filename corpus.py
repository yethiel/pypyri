import os
import requests
from bs4 import BeautifulSoup

from structure import *

class Corpus:
  
    def __init__(self):
        pass


    """ Returns the URL of the document based on the index
        The base search query includes all documents listed in the Leipzig collection
    """
    def get_object_url(self, index: int) -> str:
        url = f"https://papyri.uni-leipzig.de/servlets/solr/select?q=%2B%28objectType%3A%22schrift%22+objectType%3A%22fragment%22%29+%2Bcategory.top%3A%22Papyri_class_00000001%5C%3A0001%22&XSL.lastPage.SESSION=%2Fsearch_form_complex.xed&fl=id%2CreturnId%2CobjectType&sort=papdata_stinvent+asc&version=4.5&mask=import_search_form_complex.xed&start={index}&fl=id,returnId&rows=1&XSL.Style=browse&origrows=25"
        return url

    """ Gets the URL to the XML document found one object page 
    """
    def get_object_xml_url(self, url: str):
        page = requests.get(url)
        soup = BeautifulSoup(page.content, 'html.parser')

        # Selects the link element via css 'starts with' selector
        link_element = soup.select_one("a[href^='https://papyri.uni-leipzig.de/receive/UBLPapyri_']")

        if link_element:
            return link_element["href"]
        else:
            print(f"Error: No link element in {url}")

    """ Returns the UBLPapyri part of the XML URL 
    """
    def get_object_id(self, xml_url: str) -> str:
        return xml_url.split("receive/")[-1].split("?XSL")[0]

    """ Downloads the XML document and returns a bytes object
    """
    def get_object_xml(self, xml_url: str) -> bytes:
        req = requests.get(xml_url)
        xml = req.content

        return xml

    """ Builds a corpus by downloading all object metadata from the papyrus.uni-leipzig.de site
    """
    def build(self):
        num_entries = 5138  # number of objects in the Leipzig collection

        errors = []

        if not os.path.isdir("corpus"):
            os.mkdir("corpus")

        for idx in range(0, num_entries):
            url = self.get_object_url(idx)
            xml_url = self.get_object_xml_url(url)
            if xml_url:
                document_id = self.get_object_id(xml_url)
                print("Getting ",self.get_object_id(xml_url), f" ({idx+1}/{num_entries}, {(idx/num_entries) * 100}%)")
            else:
                errors.append(f"Error: No XML for {url}")
                continue
            
            xml = self.get_object_xml(xml_url)
            
            if not xml:
                print(f"Error getting XML for {document_id}")
                errors.append(document_id)
                continue

            with open(os.path.join("corpus", f"{document_id}.xml"), "wb") as f:
                f.write(xml)

        if (errors):
            print("Errors:")
            print("\n".join(errors))
        else:
            print("Completed without errors")


    """ Removes duplicate data
    """
    def clean(self):

        referenced_fragments = []

        for f in os.listdir("corpus"):
            with open(os.path.join("corpus", f), "r") as ff:
                # print(f)
                struc = Structure(ff.read())
                sid = struc.get_id()
                if not sid in f:
                    exit()
                # print(f"  id: {struc.get_id()}")
                # print(f"  Type: {struc.get_type()}")
                # print(f"  Text: {struc.contains_text_data()}")
                # print(f"  Frac: {struc.contains_fragments()}")
                # print(f"  F id: {struc.get_fragment_ids()}")

                referenced_fragments += struc.get_fragment_ids()
        



        external_frags = [f.split(".")[0] for f in os.listdir("corpus") if "fragment" in f]
        frag_external_count = len(external_frags)
        frag_duplicate_count = 0
        for frag in external_frags:
            if frag in referenced_fragments:  # the frag is also referenced in a schrift
                frag_duplicate_count += 1
            else:  # the fragment is not referenced in a schrift
                print("unique", frag)

        print(f"{frag_duplicate_count} / {frag_external_count}")

        

                # print(struc.contains_text_data())
                # print(struc.contains_text_data())

            
if __name__ == "__main__":
    corpus = Corpus()
    # corpus.build()
    corpus.clean()