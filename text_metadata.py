from metadata import Metadata

class TextMetadata(Metadata):


    def __init__(self):
        self.fields = {
            "title": "te01", 
            "inventory_number": "te02", 
            "preservation": "te03", 
            "preservation_text": "te04", 
            "genre": "te05", 
            "type": "te06", 
            "content": "te07", 
            "place": "te08", 
            "archive_link": "te09", 
            "date_numeric": "te10", 
            "date_detailed": "te11", 
            "date_suggestion": "te12", 
            "textual_connections": "te13", 
            "number_columns": "te14", 
            "number_lines": "te15", 
            "ink": "te16", 
            "letter_height": "te17", 
            "line_length": "te18", 
            "line_spacing": "te19", 
            "column_height": "te20", 
            "font": "te21", 
            "script_direction": "te22", 
            "layout_features": "te23", 
            "illustrations": "te24", 
            "page_flow": "te25", 
            "editio_princeps": "te26", 
            "other_editions": "te27", 
            "text_collections": "te28", 
            "text_published": "te29", 
            "text_internal": "te30", 
            "translation_published": "te31",  
            "translation_internal": "te32", 
            "correction_published": "te33", 
            "correction_internal": "te34", 
            "bibliographical_references": "te35",  
            "bibliographical_references_internal": "te36",  
            "editors": "te37", 
            "permission_publish": "te38", 
            "remarks": "te39", 
            "remarks_internal": "te40", 
            "publication": "te41", 
            "publication_number": "te42",  
            "woid": "te43", 
            "author_record": "te44", 
            "script": "te46", 
            "language": "te47", 
            "language_description": "te48", 
            "status": "te49",
        }


   
            
# with open("corpus/UBLPapyri_schrift_00098700.xml", "r") as f:
#     meta = TextMetadata()
#     meta.from_soup(BeautifulSoup(f.read(), "xml"))
    



