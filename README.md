# PyPyri

Python scripts for working with meta data from the Papyrus Projekt (Leipzig)

Metadata from fragments and written objects can be obtained from the [Papyrus Projekt](https://papyri.uni-leipzig.de/).

`corpus.py` offers a way to download metadata from all searchresults by URL

`structure.py` can be used to analyze the structure of the downloaded XML file

`*metadata.py` makes it easier to work with the metadata by translating the tag names (e.g te01 to "title")
