from bs4 import BeautifulSoup

""" This class is for making sense of MyCoRe structure XML files that contain metadata 
    for papyrus objects, written objects (schrift) or fragments (fragment).
    The structures can have an mcrstructure (schrift) or a mycoreobject (fragments) as their root node.
"""
class Structure:

    def __init__(self, xml: str):
        self.data = BeautifulSoup(xml, "xml")

        # The first tag defines the structure of the xml document
        self.structure_type = self.data.contents[0].name


    """ Returns the id with which the XML was received.
        Empirically, this matches the ID in the filename exactly.
    """ 
    def get_id(self) -> str:
        # There is only one mycoreobject in the file
        if self.structure_type == "mycoreobject":
            return self.get_tag_attribute(self.data.contents[0], "id")
        
        # Finds the id of the first mycoreobject
        elif self.structure_type == "mcrstructure":
            return self.get_tag_attribute(self.data.find("mycoreobject"), "id")


    """ Gets the object type
    """
    def get_type(self) -> str:

        # It is an aggregated papyrus object (consisting of fracments or containing text)
        if self.structure_type == "mcrstructure":  
            return self.data.find("mcritem")["type"]

        # It is a fragment, text, or any other single mycoreobject (no text data, no further children)
        elif self.structure_type == "mycoreobject":
            item_id = self.get_id()
            # Gets the type from the MyCoRe id
            return item_id.split("_")[1]


    """ Returns the attribute value of a tag.
        The case is not consistent for tag attributes which this function addresses.
    """
    def get_tag_attribute(self, tag: object, attribute: str) -> str:

        if tag.has_attr(attribute):
            return tag[attribute]
        elif tag.has_attr(attribute.upper()):
            return tag[attribute.upper()]
        elif tag.has_attr(attribute.lower()):
            return tag[attribute.lower()]

        
    """ Returns True if the object contains text data
    """
    def contains_text_data(self) -> bool:
        text_data = self.data.findAll("mcradditional", {"type": "text"})

        return text_data is not None


    def get_text_data(self) -> list:
        text_data = self.data.findAll("mcradditional", {"type": "text"})
        return text_data



    """ Returns a list of fragments
    """
    def get_fragments(self) -> list:
        return self.data.findAll("mcritemchild", {"type": "fragment"})


    """ Returns True if the object consists of fragments
    """
    def contains_fragments(self) -> bool:
        if self.get_fragments():
            return True
        else:
            return False


    """ Returns a list of contained fragment IDs (MyCoRe-IDs)
    """
    def get_fragment_ids(self) -> list:
        return [self.get_tag_attribute(f.find("mycoreobject"), "id") for f in self.get_fragments()]