import json

def get_known_dates():

    dates = {}

    # Opens the corpus file to read JSON data from
    with open("corpus.json") as fj:
        data = json.load(fj)
        # iterates over all IDs
        for internal_id in data:
            metadata = data[internal_id]
            if "text" in metadata:
                if "date_int_from" in metadata["text"] and "date_from" in metadata["text"]:
                    dates[metadata["text"]["date_int_from"]] = metadata["text"]["date_from"]
                if "date_int_to" in metadata["text"] and "date_to" in metadata["text"]:
                    dates[metadata["text"]["date_int_to"]] = metadata["text"]["date_to"]
    
    return dates

known_dates = get_known_dates()

def int_to_date(date_int):

    closest = list(known_dates.keys())[0]

    for date in known_dates:
        if abs(date_int - date) <= abs(date_int - closest):
            closest = date
    
    return known_dates[closest]


